import React from "react";
import "./main.css";
import { Navbar, Content, Footer } from "../../components";

interface MainProps {}
interface MainState {}

export class Main extends React.Component<MainProps, MainState> {
  render() {
    return (
      <React.Fragment>
        <Navbar></Navbar>
        <Content></Content>
        <Footer></Footer>
      </React.Fragment>
    );
  }
}

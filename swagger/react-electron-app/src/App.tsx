import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import { Main } from "./pages";

const App: React.FC = () => {
  return (
    <React.Fragment>
      <div className="container-fluid">
        <Router>
          <Route exact path="/" component={Main} />
        </Router>
      </div>
    </React.Fragment>
  );
};

export default App;

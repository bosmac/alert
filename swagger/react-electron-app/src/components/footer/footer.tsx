import React from "react";
import "./footer.css";

interface FooterProps {}
interface FooterState {}

export class Footer extends React.Component<FooterProps, FooterState> {
  render() {
    return (
      <React.Fragment>
        <footer className="footer">
          <div className="container">
            <span>Copyright Monster Worldwide.</span>
          </div>
        </footer>
      </React.Fragment>
    );
  }
}

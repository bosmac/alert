import React from "react";
import "./dropdown.css";

interface DropdownProps {
  list: string[];
}
interface DropdownPropsState {
  list: string[];
  selected: string;
}

export class Dropdown extends React.Component<
  DropdownProps,
  DropdownPropsState
> {
  constructor(props: DropdownProps) {
    super(props);
    this.state = { list: props.list, selected: "" };
  }
  render() {
    return (
      <React.Fragment>
        <div>
          <select className="form-control" id="exampleFormControlSelect1">
            {this.state.list &&
              this.state.list.map((item) => <option>{item}</option>)}
          </select>
        </div>
      </React.Fragment>
    );
  }
}

import React from "react";
import "./content.css";
import { Dropdown } from "../dropdown";
import { envs, services } from "./props";

interface ContentProps {
  envList?: string[];
  serviceList?: string[];
}
interface ContentState {
  envList: string[];
  serviceList: string[];
  branchName: string;
  serviceName: string;
  envName: string;
  swaggerName: string;
}

export class Content extends React.Component<ContentProps, ContentState> {
  constructor(props: ContentProps) {
    super(props);
    this.state = {
      envList: envs,
      serviceList: services,
      branchName: "",
      serviceName: services[0],
      envName: envs[0],
      swaggerName: "/swagger-ui.html",
    };
  }
  render() {
    return (
      <React.Fragment>
        <table className="table">
          <thead>
            <tr>
              <th className="col-1" scope="col">
                Branch
              </th>
              <th className="col-2" scope="col">
                Service
              </th>
              <th className="col-2" scope="col">
                Env
              </th>
              <th className="col-3" scope="col">
                Launch
              </th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>
                <input
                  type="email"
                  className="form-control"
                  id="branchName"
                  aria-describedby="branchName"
                  placeholder="branch name"
                ></input>
              </td>
              <td>
                <Dropdown list={this.state.serviceList}></Dropdown>
              </td>
              <td>
                <Dropdown list={this.state.envList}></Dropdown>
              </td>
              <td>
                <a
                  className="btn btn-outline-primary"
                  href={
                    "https://" +
                    this.state.serviceName +
                    "." +
                    this.state.envName +
                    this.state.swaggerName
                  }
                  target="_blank"
                >
                  Swagger
                </a>
              </td>
            </tr>
          </tbody>
        </table>
      </React.Fragment>
    );
  }
}

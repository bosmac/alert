export const envs: string[] = [
  "mwwnxtdevus.jobs.com",
  "mwwnxtpreprod.jobs.com",
  "mwwnxtprod.jobs.com",
];

export const services: string[] = [
  "jobs-ats-domain-service",
  "jobs-jobadapter-service-v2",
  "jobs-conversion-service-v2",
  "jobs-fraud-service",
  "jobs-inboundjobprocessing-service",
  "jobs-languagedetection-service",
  "jobs-job-api",
  "jobs-jobdiff-service",
  "jobs-job-service",
  "jobs-lookupvalues-service",
  "jobs-nondiscrimination-service",
  "jobs-org-branding-service",
  "jobs-pie-service",
  "jobs-pricing-service",
  "jobs-provider-service",
  "jobs-sanitization-service",
  "jobs-urlredirectresolver-service",
  "jobs-validation-service",
];

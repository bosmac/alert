import React from "react";
import "./navbar.css";
import logo from "../../assets/monster.png";

interface NavbarProps {}
interface NavbarState {}

export class Navbar extends React.Component<NavbarProps, NavbarState> {
  render() {
    return (
      <React.Fragment>
        <nav className="navbar navbar-dark fixed-top">
          <a className="navbar-brand" href="#">
            <img src={logo} alt="Logo" />{" "}
          </a>
        </nav>
      </React.Fragment>
    );
  }
}

package com.example.demo.service;

import com.example.demo.dto.InputMessage;
import com.example.demo.dto.OutputMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DemoService {
    @Autowired
    public DemoService(){}

    public OutputMessage notify(InputMessage inputMessage) throws Exception {
        if (inputMessage.getStateChanged()){
            Thread.sleep(3000);
            return new OutputMessage(true);
        }
        return new OutputMessage(false);
    }

}

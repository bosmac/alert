package com.example.demo.controller;

import com.example.demo.dto.InputMessage;
import com.example.demo.dto.OutputMessage;
import com.example.demo.service.DemoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;

@Controller
public class DemoController {
    private DemoService demoService;
    @Autowired
    public DemoController(DemoService demoService){
        this.demoService = demoService;
    }

    @MessageMapping("/notify")
    @SendTo("/topic/notification")
    public OutputMessage notify(InputMessage inputMessage) throws Exception {
        return this.demoService.notify(inputMessage);
    }

}

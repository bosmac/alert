package com.example.demo;

import com.example.demo.dto.InputMessage;
import com.example.demo.dto.OutputMessage;
import com.example.demo.service.DemoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xmlunit.builder.Input;

public class DemoServiceTest {

    @Test
    public void notifyTest() throws Exception {
        InputMessage inputMessage = new InputMessage(true);
        DemoService demoService = new DemoService();
        OutputMessage outputMessage = demoService.notify(inputMessage);
        Assertions.assertTrue(outputMessage.getStateChanged());
    }

}

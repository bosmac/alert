package com.example.demo;

import com.example.demo.controller.DemoController;
import com.example.demo.dto.InputMessage;
import com.example.demo.dto.OutputMessage;
import com.example.demo.service.DemoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.mockito.Mockito;

public class DemoControllerTest {
    private DemoService demoService = Mockito.mock(DemoService.class);
    private DemoController demoController = new DemoController(demoService);

    @Test
    public void notifyTest() throws Exception{
        InputMessage inputMessage = new InputMessage(true);
        OutputMessage outputMessage = new OutputMessage(true);
        Mockito.when(demoService.notify(Mockito.any())).thenReturn(outputMessage);
        OutputMessage result = demoController.notify(inputMessage);
        Assertions.assertTrue(result.getStateChanged());
    }
}

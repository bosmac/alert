1. clone repo.

2. navigate to project dir
cd alert

3. build spring app
./mvnw clean install

4. build angular app with
cd ./angular/demo
npm install

5. run spring boot app
cd to project root dir
java -jar ./target/demo-0.0.1-SNAPSHOT.jar

6. run angular app
cd <project root dir>/angular/demo
ng serve --open

7. Then visit url to view traffic light change every 3 sec
http://localhost:4200/

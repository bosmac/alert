export const service_check = {
  check_run: "https://api.datadoghq.com/api/v1/check_run?api_key=<YOUR_API_KEY>"
};

export const comments = {
  create: "https://api.datadoghq.com/api/v1/comments",
  update: "https://api.datadoghq.com/api/v1/comments/${comment_id}"
};

export const dashboard = {
  create: "https://api.datadoghq.com/api/v1/dashboard",
  update: "https://api.datadoghq.com/api/v1/dashboard/${dashboard_id}",
  delete: "https://api.datadoghq.com/api/v1/dashboard/${dashboard_id}",
  get_one: "https://api.datadoghq.com/api/v1/dashboard/${dashboard_id}",
  get_all: "https://api.datadoghq.com/api/v1/dashboard"
};

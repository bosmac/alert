export const AppEngineEndpoints = {
  base: "https://appengine.googleapis.com",
  apps: {},
  apps_authorizedCertificates: {},
  apps_authorizedDomains: {},
  apps_domainMappings: {},
  apps_firewall_ingressRules: {},
  apps_locations: {},
  apps_operations: {},
  apps_services: {},
  apps_services_versions: {},
  apps_services_versions_instances: {}
};

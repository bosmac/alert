export const KubernetesEngineEndpoints = {
  base: "https://container_googleapis_com",
  projects_aggregated_usableSubnetworks: {},
  rojects_locations: {},
  projects_locations_clusters: {},
  projects_locations_clusters_nodePools: {},
  projects_locations_clusters_well_known: {},
  projects_locations_operations: {},
  projects_zones: {},
  projects_zones_clusters: {},
  projects_zones_clusters_nodePools: {},
  projects_zones_operations: {}
};

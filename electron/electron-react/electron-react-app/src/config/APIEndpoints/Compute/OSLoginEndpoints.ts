export const OSLoginEndpoints = {
  base: "https://oslogin.googleapis.com",
  users: {},
  users_projects: {},
  users_sshPublicKeys: {}
};

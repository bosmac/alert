export const CloudBigtableAdminEndpoints = {
  base: "https://appengine_googleapis_com",
  operations: {},
  operations_projects_operations: {},
  projects_instances: {},
  projects_instances_appProfiles: {},
  projects_instances_clusters: {},
  projects_instances_tables: {},
  projects_locations: {}
};

import React from "react";
import "./content.css";
import * as child from "child_process";

interface ContentProps {
  name: string;
  url: string;
}
interface ContentState {}

export class Content extends React.Component<ContentProps, ContentState> {
  render() {
    return (
      <React.Fragment>
        <a className="btn btn-outline-primary mt-5 pt-5" href={this.props.url}>
          Launch
        </a>
      </React.Fragment>
    );
  }
}

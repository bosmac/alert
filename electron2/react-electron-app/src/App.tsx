import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";

const App: React.FC = () => {
  return (
    <React.Fragment>
      <div className="container-fluid">
        <Router>
          <Route
            exact
            path="/"
            component={() => {
              window.location.href = "https://www.monster.com/";
              return null;
            }}
          />
        </Router>
      </div>
    </React.Fragment>
  );
};

export default App;

import { Component, OnInit } from '@angular/core';
import * as Stomp from 'stompjs';
import * as SockJS from 'sockjs-client';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  webSocketEndPoint: string = 'http://localhost:8080/ws';
  topic: string = "/topic/notification";
  stompClient: any;
  
  title:string = 'Traffic Light Demo';
  redCircle:string = "red";
  yellowCircle:string = "black";
  greenCircle:string = "black";
  current:number = 1;
  
  ngOnInit() {
    this.connect();
  }

  connect() {
    let ws = new SockJS(this.webSocketEndPoint);
    this.stompClient = Stomp.over(ws);
    const _this = this;
    _this.stompClient.connect({}, function (frame:any) {
        _this.onConnectionEstablished();
        _this.stompClient.subscribe(_this.topic, function (message:any) {
            _this.onMessageReceived();
        });
    }, this.errorCallBack);
  };

  disconnect() {
    if (this.stompClient !== null) {
        this.stompClient.disconnect();
    }
  }

  errorCallBack(error:any) {
      setTimeout(() => {
          this.connect();
      }, 5000);
  }

  sendMessage() {
      let stateChanged = true;
      this.stompClient.send("/app/notify", {}, JSON.stringify(stateChanged));
  }

  onConnectionEstablished(){
    setTimeout(() => {
      this.sendMessage();
    }, 2000);
  }

  onMessageReceived() {
    switch(this.current) {
      case 1: {
        this.redCircle = "black";
        this.yellowCircle = "black";
        this.greenCircle = "green";
        this.current = 2; 
        this.sendMessage();
        break; 
      } 
      case 2: {
        this.redCircle = "black";
        this.yellowCircle = "yellow";
        this.greenCircle = "black";
        this.current = 3; 
        this.sendMessage();
        break; 
      } 
      case 3: {
        this.redCircle = "red";
        this.yellowCircle = "black";
        this.greenCircle = "black";
        this.current = 1; 
        this.sendMessage();
        break; 
     } 
    } 
  }
}
